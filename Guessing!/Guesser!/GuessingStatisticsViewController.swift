//
//  ThirdViewController.swift
//  Guessing!
//
//  Created by student on 2/27/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class GuessingStatisticsViewController: UIViewController {

    
    @IBOutlet weak var maxLBL: UILabel!
    @IBOutlet weak var minLBL: UILabel!
    @IBOutlet weak var meanLBL: UILabel!
    @IBOutlet weak var stdevLBL: UILabel!
    
    @IBAction func clearStatisticsBTN(_ sender: Any) {
        minLBL.text = "0"
        maxLBL.text = "0"
        meanLBL.text = "0.0"
        stdevLBL.text = "0.0"
        Guesser.shared.clearStatistics()
    }
    var sum = 0
    var std = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        constraints()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
     constraints()
   }
    func constraints(){
        minLBL.text = String(Guesser.shared.minNumAttempts())
        maxLBL.text = String(Guesser.shared.maxNumAttempts())
        for i in 0..<Guesser.shared.numGuesses(){
            sum = sum + Guesser.shared.guess(index: i).numAttemptsRequired
        }
        let mean = Double(sum)/Double(Guesser.shared.numGuesses())
        meanLBL.text = "\(mean)"
        for i in 0..<Guesser.shared.numGuesses(){
            std = std + pow(Double(Guesser.shared.guess(index: i).numAttemptsRequired) - mean , 2)
        }
        stdevLBL.text = "\(std/Double(Guesser.shared.numGuesses()))"
    }

}
