//
//  SecondViewController.swift
//  Guessing!
//
//  Created by student on 2/27/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class GuessingHistoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return Guesser.shared.numGuesses()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let returingval = tableView.dequeueReusableCell(withIdentifier: "cell1")!
        returingval.textLabel?.text = "correct Answer" + String(Guesser.shared[indexPath.row].correctAnswer)
        returingval.detailTextLabel?.text = "# Attempts" + String(Guesser.shared[indexPath.row].numAttemptsRequired)
     return returingval
    }
    
    @IBOutlet weak var historyTableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        historyTableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        historyTableView.dataSource = self
        historyTableView.delegate = self
        
    }

}

